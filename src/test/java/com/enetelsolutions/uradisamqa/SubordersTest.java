package com.enetelsolutions.uradisamqa;

import org.testng.annotations.Test;

import java.util.List;

public class SubordersTest extends InitTest{

    List<CartItem> cartItems;
    Product product1;
    Product product2;
    Product product3;
    Product product4;
    Product product5;
    Product product6;
    Product product7;

    String max1 = "135";
    String max2 = "1080";
    String max3 = "75";
    String max4 = "20";
    String max5 = "44";
    String max6 = "47";
    String max7 = "23";

    String shipment1 = "Pošiljka 1 po porudžbini pristiže iz PJ Novi Sad";
    String shipment2 = "Pošiljka 2 po porudžbini pristiže iz PJ Kruševac";
    String shipment3 = "Pošiljka 3 po porudžbini pristiže iz PJ Novi Beograd - Roda Centar";
    String shipment4 = "Pošiljka 4 po porudžbini pristiže iz PJ Beograd - Vidikovac";
    String shipment5 = "Pošiljka 5 po porudžbini pristiže iz PJ Zemun";
    String shipment6 = "Pošiljka 6 po porudžbini pristiže iz PJ Zrenjanin";
    String shipment7 = "Pošiljka 7 po porudžbini pristiže iz PJ Beograd - Karaburma";
    String shipment8 = "Pošiljka 8 po porudžbini pristiže iz PJ Pančevo";
    String shipment9 = "Pošiljka 9 po porudžbini pristiže iz PJ Čačak";
    String shipment10 = "Pošiljka 10 po porudžbini pristiže iz PJ Šabac";
    String shipment11 = "Pošiljka 1 po porudžbini pristiže iz PJ Kruševac";
    String shipment12 = "Pošiljka 1 po porudžbini pristiže iz PJ Zemun";

    public static double totalS2;


    @Test(description="Correct shop allocation")
    public void s1correctShopAllocationMaxQty() { // US-579, US-742, US-785 - potrebno prosiriti za Marijanin case
        driver.get(BASE_URL + url.p5);
        spp.cookieAgree();
        product1 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p6);
        product2 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p7);
        product3 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p8);
        product4 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p9);
        product5 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p10);
        product6 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p11);
        product7 = spp.addToCartFromSPP();

        spp.goToCart();

        cart.changeQtyInCart(product1.productName, max1);
        cart.changeQtyInCart(product2.productName, max2);
        cart.changeQtyInCart(product3.productName, max3);
        cart.changeQtyInCart(product4.productName, max4);
        cart.changeQtyInCart(product5.productName, max5);
        cart.changeQtyInCart(product6.productName, max6);
        cart.changeQtyInCart(product7.productName, max7);

        cart.goToCartPreview();

        sa.assertEquals(cpp.getSuborderCities(1), shipment1);
        sa.assertEquals(cpp.getSuborderCities(2), shipment2);
        sa.assertEquals(cpp.getSuborderCities(3), shipment3);
        sa.assertEquals(cpp.getSuborderCities(4), shipment4);
        sa.assertEquals(cpp.getSuborderCities(5), shipment5);
        sa.assertEquals(cpp.getSuborderCities(6), shipment6);
        sa.assertEquals(cpp.getSuborderCities(7), shipment7);
        sa.assertEquals(cpp.getSuborderCities(8), shipment8);
        sa.assertEquals(cpp.getSuborderCities(9), shipment9);
        sa.assertEquals(cpp.getSuborderCities(10), shipment10);

        sa.assertTrue(cpp.findProductInSuborders(product4.productName));
//          potrebno prosiriti da kontrolise kolicinu koja je u suborderu i koja je istaknuta kao neraspoloziva ili nemoguca za isporuku
        sa.assertTrue(cpp.findBlacklistedProductInCart(product4.productName)); // US-777
        sa.assertTrue(cpp.findPickUpProductInCart(product6.productName));

        cpp.getBasketValues();

        sa.assertEquals(cpp.orderProductsTotal, cpp.sumProductsTotal());
        sa.assertEquals(cpp.orderProductsTotal, cpp.sumSuborderProductTotal());
        sa.assertEquals(cpp.orderShippingTotal, cpp.sumShippingPrices());
        sa.assertEquals(cpp.orderTotalAmount, cpp.sumSuborderTotal());
        sa.assertTrue(cpp.totalOrderVerification());

        cpp.goToCheckoutFromPreview();
        checkout.fillFormPersonal();
        checkout.citySelect("Tem");
        checkout.paymentSelection(1, "f");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());

        sa.assertTrue(ty.shipmentShopValidation(1, shipment1));
        sa.assertTrue(ty.shipmentShopValidation(2, shipment2));
        sa.assertTrue(ty.shipmentShopValidation(3, shipment3));
        sa.assertTrue(ty.shipmentShopValidation(4, shipment4));
        sa.assertTrue(ty.shipmentShopValidation(5, shipment5));
        sa.assertTrue(ty.shipmentShopValidation(6, shipment6));
        sa.assertTrue(ty.shipmentShopValidation(7, shipment7));
        sa.assertTrue(ty.shipmentShopValidation(8, shipment8));
        sa.assertTrue(ty.shipmentShopValidation(9, shipment9));
        sa.assertTrue(ty.shipmentShopValidation(10, shipment10));


        sa.assertAll();

    }

    @Test(description="Correct shop allocation")
    public void s2correctShopAllocation() { // US-579, US-742, US-785 - potrebno prosiriti za Marijanin case
        driver.get(BASE_URL + url.p14);
        product1 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p15);
        product2 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p16);
        product3 = spp.addToCartFromSPP();


        spp.goToCart();

        cart.goToCartPreview();

        sa.assertEquals(cpp.getSuborderCities(1), shipment11);


        sa.assertTrue(cpp.findProductInSuborders(product1.productName));
//          potrebno prosiriti da kontrolise kolicinu koja je u suborderu i koja je istaknuta kao neraspoloziva ili nemoguca za isporuku



        cpp.getBasketValues();

        sa.assertEquals(cpp.orderProductsTotal, cpp.sumProductsTotal());
        sa.assertEquals(cpp.orderProductsTotal, cpp.sumSuborderProductTotal());
        sa.assertEquals(cpp.orderShippingTotal, cpp.sumShippingPrices());
        sa.assertEquals(cpp.orderTotalAmount, cpp.sumSuborderTotal());
        sa.assertTrue(cpp.totalOrderVerification());

        totalS2 = cpp.orderTotalAmount;

        cpp.goBackToCartPage();
        cart.changeQtyInCart(product1.productName, "2");
        cart.goToCartPreview();
        sa.assertEquals(cpp.getSuborderCities(1), shipment11);
        cpp.getBasketValues();
        sa.assertNotEquals(cpp.orderTotalAmount, totalS2);
        totalS2 = cpp.orderTotalAmount;

        cpp.goBackToCartPage();
        cart.changeQtyInCart(product2.productName, "3");
        cart.goToCartPreview();
        sa.assertEquals(cpp.getSuborderCities(1), shipment12);
        cpp.getBasketValues();
        sa.assertNotEquals(cpp.orderTotalAmount, totalS2);
        totalS2 = cpp.orderTotalAmount;

        cpp.goToCheckoutFromPreview();
        checkout.fillFormPersonal();
        checkout.citySelect("Tem");
        checkout.paymentSelection(1, "f");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());

        sa.assertTrue(ty.shipmentShopValidation(1, shipment12));

        sa.assertAll();


    }

    @Test(description="Pick up products don't appear on preview")
    public void s2emptyPreviewPage() { // US-492
        driver.get(BASE_URL + url.p4);
        product1 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p12);
        product2 = spp.addToCartFromSPP();
        driver.get(BASE_URL + url.p13);
        product3 = spp.addToCartFromSPP();


        spp.goToCart();

        cart.goToCartPreview();

        sa.assertTrue(cpp.findPickUpProductInCart(product1.productName));
        sa.assertTrue(cpp.findPickUpProductInCart(product2.productName));
        sa.assertTrue(cpp.findPickUpProductInCart(product3.productName));

        cpp.getBasketValues();

        sa.assertEquals(cpp.orderProductsTotal, 0.0);
        sa.assertEquals(cpp.orderShippingTotal, 0.0);
        sa.assertEquals(cpp.orderTotalAmount, 0.0);
        sa.assertTrue(cpp.totalOrderVerification());

        sa.assertAll();

    }

    @Test(description="Cannot buy max but only available qty of blacklisted item")
    public void s3blacklistedProduct() { // US-796
        driver.get(BASE_URL + url.p11);
        product4 = spp.addToCartFromSPP();

        spp.goToCart();

        cart.changeQtyInCart(product4.productName, max7);

        cart.goToCartPreview();

        sa.assertTrue(cpp.findPickUpProductInCart(product1.productName));
        sa.assertTrue(cpp.findPickUpProductInCart(product2.productName));
        sa.assertTrue(cpp.findPickUpProductInCart(product3.productName));
        sa.assertTrue(cpp.findBlacklistedProductInCart(product4.productName));


        cpp.getBasketValues();

        sa.assertEquals(cpp.orderProductsTotal, cpp.sumProductsTotal());
        sa.assertEquals(cpp.orderProductsTotal, cpp.sumSuborderProductTotal());
        sa.assertEquals(cpp.orderShippingTotal, cpp.sumShippingPrices());
        sa.assertEquals(cpp.orderTotalAmount, cpp.sumSuborderTotal());
        sa.assertTrue(cpp.totalOrderVerification());

        cpp.goToCheckoutFromPreview();
        checkout.fillFormPersonal();
        checkout.citySelect("Tem");
        checkout.paymentSelection(1, "f");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());

        sa.assertAll();

    }



}
