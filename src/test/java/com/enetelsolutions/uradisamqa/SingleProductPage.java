package com.enetelsolutions.uradisamqa;


import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class SingleProductPage extends MainPage {

	public SingleProductPage(WebDriver driver) {
		super(driver);
	}


	@FindBy(className = "product_code")
	WebElement productCode;

	@FindBy(css = "h1[id*='fnc-product-name']")
	WebElement productName;

	@FindBy(css = ".single-product-price")
	WebElement productPrice;
	
	@FindBy(css = "div.product-info-reverse div.fnc-cart-btn") 
	WebElement addToCart;

	


	// Pregledani i slicni proizvodi

	// Add to cart btn

	@FindBy(css = ".promo-slider-1 .slick-active .fnc-cart-btn")
	List<WebElement> addToCartViewed;

	@FindBy(css = ".promo-slider-2 .slick-active .fnc-cart-btn")
	List<WebElement> addToCartSimilar;

	// Product codes

	 @FindBy(css = ".promo-slider-1 .product-code")
	 List<WebElement> productCodeViewed;

	 @FindBy(css = ".promo-slider-2 .product-code")
	 List<WebElement> productCodeSimilar;


	// Product names

	@FindBy(css = ".promo-slider-1 .slick-active  h3 a")
	List<WebElement> productNameViewed;

	@FindBy(css = ".promo-slider-2 .slick-active  h3 a")
	List<WebElement> productNameSimilar;

	
	// Product prices

	@FindBy(css = ".promo-slider-1 .slick-active  .product-price")
	List<WebElement> productPriceViewed;

	@FindBy(css = ".promo-slider-2 .slick-active  .product-price")
	List<WebElement> productPriceSimilar;




	// Methods

	@Step("Adding to cart from spp")
	public Product addToCartFromSPP () {
		product = new Product();
		product.productCode = productCode.getText(); // kod proizvoda
		product.productName = productName.getText(); // ime proizvoda
		product.productPrice = stringToDouble(productPrice.getText()); // cena proizvoda
		addToCart.click();
		sleep(0.5);
		return product;
	}


	@Step("Adding to cart Viewed")
		public Product addToCartFromViewed(int index) {
			index = index - 1;
			product = new Product();
				if (index <= addToCartViewed.size()) {
					addToCartViewed.get(index).click();
					product.productCode = productCodeViewed.get(index).getText();
					product.productName = productNameViewed.get(index).getText();
					product.productPrice = stringToDouble(productPriceViewed.get(index).getText());
				} 
				else {
				System.out.println("You have entered wrong index number");
			}
			return product;
		}

	@Step("Adding to cart Similar")
		public Product addToCartFromSimilar(int index) {
			index = index - 1;
			product = new Product();
			if (index <= addToCartSimilar.size()) {
				addToCartSimilar.get(index).click();
				product.productCode = productCodeSimilar.get(index).getText();
				product.productName = productNameSimilar.get(index).getText();
				product.productPrice = stringToDouble(productPriceSimilar.get(index).getText());
			} else {
				System.out.println("You have entered wrong index number");
			}
			return product;
		}

	

	
}
