package com.enetelsolutions.uradisamqa;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;


public class ThankYouPage extends MainPage {

    public ThankYouPage(WebDriver driver) {
        super(driver);
    }


    // double orderDiscount;
    double orderShippingTotal;
    double orderTotalAmount;


    @FindBy(css = "h1.thank-you-title")
    WebElement thankYouTitle;

    @FindBy(css= ".btn-primary")
    WebElement backToShop;

    @FindBy(css = ".typ_order_specification .spec_code")
    WebElement orderSpec;

    @FindBy(css = ".typ_order_specification .product_name")
    List<WebElement> suborderItemName;

    @FindBy(className = "order_item_price")
    List<WebElement> suborderItemQty;

    @FindBy(className = "order_item_final_price")
    List<WebElement> suborderItemTotalPrice;



    // Suborders

    @FindBy(className = "ordet_item_address")
    List<WebElement> suborderCity;

    @FindBy(css = ".suborder-product-total .suborder-price")
    List<WebElement> subsuborderPrices;

    @FindBy(css = ".suborder-shipping-total .suborder-price")
    List<WebElement> subsubordersShipping;

    @FindBy(css = ".suborder-price-total .suborder-final-price")
    List<WebElement> subsubordersTotalAmount;


    // Total

    @FindBy(className = "spec_price_two_total")
    List<WebElement> shippingAndTotal;


    public ThankYouPage getTYValues() {

        ThankYouPage ty = new ThankYouPage(driver);

        // this.orderDiscount = decimalToDouble(totalDiscount.getText()); coupons have to be resolved
        this.orderShippingTotal = decimalToDouble(shippingAndTotal.get(0).getText());
        this.orderTotalAmount = decimalToDouble(shippingAndTotal.get(1).getText());

//        System.out.println("TY Order Shipping: " + orderShippingTotal);
//        System.out.println("TY Order Total Amount: " + orderTotalAmount);

        return ty;
    }


    @Step("Getting order code")
    public String getOrderSpecValue() {
        String orderCode = orderSpec.getText();
        orderCode = orderCode.substring(30);
        return orderCode;
    }

    @Step("Getting suborder city")
    public String getSuborderCities(int i) {
        i = i - 1;
        return suborderCity.get(i).getText();
    }

    @Step("City shop validation")
    public boolean shipmentShopValidation (int i, String s) {
        String cityShop = getSuborderCities(i);
//        System.out.println(cityShop);
//        System.out.println(s.substring(41));

        if (cityShop.contains(s.substring(41))) {
            return true;
        }
        else {
            return false;
        }
    }



    @Step("Verify products total via items") // prolazi kroz iteme i sabira cene
    public double sumProductsTotal() {
        double productsTotal = 0;
        for (int i = 0; i < suborderItemName.size(); i++) {
            double total = decimalToDouble(suborderItemTotalPrice.get(i).getText());
            productsTotal = productsTotal + total;
        }
        //	System.out.println("Sum Products Total: " + productsTotal);
        return productsTotal;
    }

    @Step("Verify products total via suborder product prices") // prolazi kroz subordere i sabira product total
    public double sumSuborderProductTotal() {
        double suborderProductTotal = 0;
        for (int i = 0; i < subsuborderPrices.size(); i++) {
            double total = decimalToDouble(subsuborderPrices.get(i).getText());
            suborderProductTotal = suborderProductTotal + total;
        }
        //	System.out.println("Sum suborder products total: " + suborderProductTotal);
        return suborderProductTotal;
    }

    @Step("Calculating shipping total") // prolazi kroz subordere i sabira troskove isporuke
    public double sumShippingPrices() {
        double shippingTotal = 0;
        for (int i = 0; i < subsubordersShipping.size(); i++) {
            double total = decimalToDouble(subsubordersShipping.get(i).getText());
            shippingTotal = shippingTotal + total;
        }
        //	System.out.println("Sum Shipping total: " + shippingTotal);
        return shippingTotal;
    }

    @Step("Verify suborders total via suborders total amount") // prolazi kroz subordere i sabira ukupan total
    public double sumSuborderTotal() {
        double suborderTotal = 0;
        for (int i = 0; i < subsubordersTotalAmount.size(); i++) {
            double total = decimalToDouble(subsubordersTotalAmount.get(i).getText());
            suborderTotal = suborderTotal + total;
        }
        //	System.out.println("Sum suborder total: " + suborderTotal);
        return suborderTotal;
    }

    @Step ("Verifying Thank You Page")
    public boolean isAtThankYouPage() {
		wait.until(ExpectedConditions.visibilityOf(thankYouTitle));
		try {
            System.out.print(getOrderSpecValue() + ", ");
        }
		catch (Exception e) {

        }
		return thankYouTitle.isDisplayed();
	}


}