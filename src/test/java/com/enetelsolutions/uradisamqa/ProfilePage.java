package com.enetelsolutions.uradisamqa;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class ProfilePage extends MainPage {

	public ProfilePage(WebDriver driver) {
		super(driver);
	}
	
	// Profile menu items

	@FindBy(css = "a.my_data")
	WebElement personal;

	@FindBy(css = "a.my_orders_link")
	WebElement myOrders;

	@FindBy(css = "a.anchor_wish_list")
	WebElement wishList;

	@FindBy(css = "a.coupons")
	WebElement coupons;

	@FindBy(css = "a.change_pass")
	WebElement changePass;

	@FindBy(css = "a.log_out")
	WebElement logOut;

	

	// Wishlist
	
	@FindBy(css=".wish-list .product") // proizvod box general lokator
	List<WebElement> productBox;

	@FindBy(css = ".product .product-code")
	List <WebElement> productCode; // iscitan kod ima space u sebi, potreban parsing

	@FindBy(css = ".product .product-image-box a")
	List <WebElement> productImageLink;

	@FindBy(css = ".product .fnc-compare-checkbox")
	List <WebElement> productCompareBtn;

	@FindBy(css = ".product h3 a")
	List <WebElement> productName;

	@FindBy(css = ".wish-list-price-info .product-price")
	List <WebElement> productPrice; // npr. 12.999 RSD potreban parsing

	@FindBy(css = "div.fnc-cart-btn")
	List <WebElement> productAddToCart;

	@FindBy(css = "a.product-wish")
	List <WebElement> removeFromWishList;


	// Coupons

	// doraditi po zavrsetku



	// Methods

	@Step("Go to orders")
	public void getMyOrders() {
		myOrders.click();
	}
	
	@Step("Go to wishlist")
	public void getWishlist() {
		wishList.click();
	}

	@Step("Go to coupons")
	public void getCoupons() {
		coupons.click();
	}
	
	@Step("Go to personal data")
	public void getPersonalData() {
		personal.click();
	}

	@Step("Go to password change")
	public void getPassChange() {
		changePass.click();
	}
	
	@Step("Log out")
	public void getLogOut() {
		logOut.click();
	}


	// Wishlist


	@Step("Getting product's code, name and price")
	public Product getProductData (int index) {
		product = new Product();
		product.productCode = productCode.get(index).getText(); // kod proizvoda
		product.productName = productName.get(index).getText(); // ime proizvoda
		product.productPrice = stringToDouble(productPrice.get(index).getText()); // cena proizvoda
		return product;
	}


	@Step("Adding to cart")
	public Product addToCart (int index) {
		index = index - 1;
		product = getProductData(index);
		productAddToCart.get(index).click();
		sleep(2);
		return product;
	}



}
