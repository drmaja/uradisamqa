package com.enetelsolutions.uradisamqa;

import io.qameta.allure.Step;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class RegistrationTest extends InitTest {


	
	@Step("Happy path case Personal")
	@Test(description="Personal registration")
		public void registrationPersonal() {
		driver.get( BASE_URL + url.loginURL);
		login.goToRegisterForm();
		register.fillFormPersonal();
		register.citySelect("Novi S");
		register.setDateOfBirth();
		register.confirm();
		assertTrue(register.isSignedIn());
		register.signOut();
	}

	@Step("Happy path case Personal")
	@Test(description="Personal registration")
	public void registrationCompany() {
		driver.get( BASE_URL + url.loginURL);
		login.cookieAgree();
		login.goToRegisterForm();
		register.selectUserType(2);
		register.fillFormCompany();
		register.citySelect("Novi S");
		register.setDateOfBirth();
		register.confirm();
		assertTrue(register.isSignedIn());
		register.signOut();
	}



}
