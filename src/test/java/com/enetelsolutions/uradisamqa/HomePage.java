package com.enetelsolutions.uradisamqa;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;

public class HomePage extends MainPage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	JavascriptExecutor js = (JavascriptExecutor) driver;

	

	// Locators

	// Promo services

	
	@FindBy(css = "div.promo-slider-1")
	WebElement promoSlider;

	@FindBy(css = "div.promo-slider-2")
	WebElement promoSlider2;

	@FindBy(css = "div.promo-slider-3")
	WebElement promoSlider3;



	// Add to cart btn

	@FindBy(css = ".promo-slider-1 .slick-active .fnc-cart-btn")
	List<WebElement> addToCartPS1;

	@FindBy(css = ".promo-slider-2 .slick-active .fnc-cart-btn")
	List<WebElement> addToCartPS2;

	@FindBy(css = ".promo-slider-3 .slick-active .fnc-cart-btn")
	List<WebElement> addToCartPS3;

	// Product codes

	@FindBy(css = ".promo-slider-1 .product-code")
	List<WebElement> productCodePS1;

	@FindBy(css = ".promo-slider-2 .product-code")
	List<WebElement> productCodePS2;

	@FindBy(css = ".promo-slider-3 .product-code")
	List<WebElement> productCodePS3;

	// Product names

	@FindBy(css = ".promo-slider-1 .slick-active  h3 a")
	List<WebElement> productNamePS1;

	@FindBy(css = ".promo-slider-2 .slick-active  h3 a")
	List<WebElement> productNamePS2;

	@FindBy(css = ".promo-slider-3 .slick-active  h3 a")
	List<WebElement> productNamePS3;

	// Product prices

	@FindBy(css = ".promo-slider-1 .slick-active  .product-price")
	List<WebElement> productPricePS1;

	@FindBy(css = ".promo-slider-2 .slick-active  .product-price")
	List<WebElement> productPricePS2;

	@FindBy(css = ".promo-slider-3 .slick-active  .product-price")
	List<WebElement> productPricePS3;

	// Show all from promo
	@FindBy(css = ".promo-slider-wrap .promo-show-all")
	List<WebElement> showAllPromo;



	// Methods

	

	@Step("Adding to cart from promo services")
		public Product addToCartFromPromoService(int ps, int index) {
			index = index - 1;
			Product product = new Product();
			if (ps == 1) {
				js.executeScript("arguments[0].scrollIntoView(true)", promoSlider);
				if (index <= addToCartPS1.size()) {
					addToCartPS1.get(index).click();
					product.productCode = productCodePS1.get(index).getText();
					product.productName = productNamePS1.get(index).getText();
					product.productPrice = stringToDouble(productPricePS1.get(index).getText()) ;
				} 
				else {
				System.out.println("You have entered wrong index number");
			} }
			else if (ps == 2) {
				js.executeScript("arguments[0].scrollIntoView(true)", promoSlider2);
				if (index <= addToCartPS2.size()) {
					addToCartPS2.get(index).click();
					product.productCode = productCodePS2.get(index).getText();
					product.productName = productNamePS2.get(index).getText();
					product.productPrice = stringToDouble(productPricePS2.get(index).getText());
					} 
				else {
					System.out.println("You have entered wrong index number");
			} }
			else if (ps == 3) {
				js.executeScript("arguments[0].scrollIntoView(true)", promoSlider3);
				if (index <= addToCartPS3.size()) {
					addToCartPS3.get(index).click();
					product.productCode = productCodePS3.get(index).getText();
					product.productName = productNamePS3.get(index).getText();
					product.productPrice = stringToDouble(productPricePS3.get(index).getText());
					} 
				else {
					System.out.println("You have entered wrong index number");
			} }
			else {
				System.out.println("Wrong promo service index number");
			}
			return product;
		}

		@Step("Accessing promo service page")
		public void showAllPromo(int index) {
			index = index - 1;
		if (index <= showAllPromo.size()) {
			
			try {
				showAllPromo.get(index).click();
			}
			catch (Exception e) {
				System.out.println("Cannot access promo service");
			}
		}
		else
			System.out.println("Ooops, wrong promo service index number");
		}

		@Step("Verifikacija pocetne stranice")
		public boolean isAtHomePage() {
			wait.until(ExpectedConditions.visibilityOf(promoSlider));
			return promoSlider.isDisplayed();
		}



		
		
}


