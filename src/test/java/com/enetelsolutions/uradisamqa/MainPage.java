package com.enetelsolutions.uradisamqa;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;

public class MainPage extends PageObject {

	public MainPage(WebDriver driver) {
		super(driver);

	}

	Date date = new Date(System.currentTimeMillis());
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	Product product;
	JavascriptExecutor js = (JavascriptExecutor) driver;

	// LOCATORS

	// Header area

	@FindBy(className = "header-logo")
	WebElement headerLogo;

	@FindBy(css = "input#fnc-search_field")
	WebElement searchField;

	@FindBy(id = "search_btn")
	WebElement searchBtn;

	@FindBy(css = "div.js-search-categories")
	WebElement allCategoriesBtn;

	@FindBy(css = "li.basket-link")
	WebElement basketHeader;

	@FindBy(id = "br_proizvoda-basket")
	WebElement itemsInBasket;

	@FindBy(css = ".basket-header-button a")
	WebElement checkoutBtn;


	@FindBy(css = "a[href *='wish_list']")
	WebElement wishList;

	@FindBy(css = ".profile-link a")
	WebElement profileIcon;


	// Footer area


	// Newsletter area

	@FindBy(id = "footer_email")
	WebElement newsletterEmailInput;

	@FindBy(css = ".newsletter-input button")
	WebElement newsletterSubmit;

	
	// Footer bottom area

	@FindBy(css= ".footer-bottom .accordion-tab a")
	List<WebElement> footerLinks;


	// Cookie bar
	@FindBy(css = "#modal-cookie-info .cookie-agree")
	WebElement cookieAgree;
	


	// METHODS

	// Common methods


	@Step("Clearing input fields")
	public void clearField(WebElement element) {
		Actions builder = new Actions(driver);
		Action clearField = builder.click(element).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL)
				.sendKeys(Keys.DELETE).build();
		clearField.perform();
	}

	@Step("Getting element text")
	public String getText(WebElement element) {
		return element.getText();
	}

	@Step("Sending keys to input field")
	public void type(String text, WebElement element) {
		Actions builder = new Actions(driver);
		Action clearField = builder.click(element).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL)
				.sendKeys(Keys.DELETE).build();
		clearField.perform();
		element.sendKeys(text);
	}

	@Step("Moves to element")
	public void moveTo(WebElement element) {

		Actions act = new Actions(driver);
		Action add = act.moveToElement(element).build();
		add.perform();
		sleep(1);
		Action click = act.click().build();
		click.perform();

	}

	@Step("Hovering element")
	public void hover(WebElement element) {
		Actions builder = new Actions(driver);
		Action hoverField = builder.moveToElement(element).build();
		hoverField.perform();
	}

	@Step("Checking if element is displayed")
	public boolean isDisplayed(WebElement element) {
		boolean display = false;
		try {
			display = element.isDisplayed();
		} catch (Exception e) {
			display = false;
		}
		return display;
	}

	@Step("Sleeping")
	public void sleep(double sec) {
		double seconds = sec * 1000;
		long millis = (long) seconds;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	// Regex and parsing methods
	public double stringToDouble (String s) {
		double number = 0;
		try {
			number = Double.parseDouble(s.replaceAll("[A-z\\s\\%\\.\\\n]*", ""));
		} catch (Exception e) {
			   System.out.println("Greska kod parsiranja cene proizvoda");
		}
       return number;
	}

	@Step("Parse price with decimal places")
	public double decimalToDouble (String s) { // modifikovan String to double metod
		double number = 0;
		try {
			String removePoint = s.replaceAll("[A-z\\s\\%\\.]*", "");
			if (removePoint.contains(",")) {
				removePoint = removePoint.replaceAll(",", ".");
			}
			number = Double.parseDouble(removePoint);
		} catch (Exception e) {
			System.out.println("Greska kod parsiranja cene proizvoda");
		}

		return number;
	}

	@Step("Parse price with decimal places")
	public double replacePointAndParceToDouble (String s) { // modifikovan String to double metod
		double number = 0;
		try {
			String removePoint = s.replaceAll("[A-z\\s\\%]*", "");
			number = Double.parseDouble(removePoint);
		} catch (Exception e) {
			System.out.println("Greska kod parsiranja cene proizvoda");
		}

		return number;
	}


	// Header area methods

	@Step("Searching for the product")
	public void search(String textsearch) {
		clearField(searchField);
		searchField.sendKeys(textsearch);
		searchBtn.click();
	}

	@Step("Counting products in cart")
	public int cartCounter() { //
		sleep(2);
		int counter;
		if (itemsInBasket.isDisplayed()) {
			String cartNumber = itemsInBasket.getText();
			counter = Integer.parseInt(cartNumber);
		} else
			counter = 0;
		return counter;
	}

	@Step("Incrementing +1 actual basket")
	public int cartCountIncrement() {
		int counter = cartCounter();
		counter++;
		return counter;
	}


	@Step("Opening basket")
	public void goToCart() {
		basketHeader.click();
		sleep(1);
//		try {
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".basket-header-button a")));
//		checkoutBtn.click();
//		} catch (Exception e) {
//			System.out.println("Basket is empty!");
//		}
	}

	@Step("Going to profile")
	public void goToProfile() {
		profileIcon.click();
	}

	@Step("Sign-out user")
	public void signOut() {
		profileIcon.click();
		ProfilePage profile = new ProfilePage(driver);
		profile.logOut.click();
	}

	


	// Footer area methods
	
	@Step("Newsletter subscribe")
	public void newsletterSubscribe(String email) {
		clearField(newsletterEmailInput);
		newsletterEmailInput.sendKeys(email);
		newsletterSubmit.click();
	}

	@Step("Visit page link")
	public void openFooterLink(String pageName) {
		int index = footerLinks.size();
		for (int i = 0; i < index; i++) {
			String page = footerLinks.get(i).getText();
			if (page.contains(pageName)) {
				footerLinks.get(i).click();
			}
			
		}
	}

	@Step("Cookie agree")
	public void cookieAgree() {
		try {
			wait.until(ExpectedConditions.visibilityOf(cookieAgree));
			cookieAgree.click();
			sleep(1);
		} catch (Exception e) {

		}
	}

	// Asserting methods

	@Step("Verifying user is signed in")
	public boolean isSignedIn() {
		ProfilePage profile = new ProfilePage(driver);
		wait.until(ExpectedConditions.visibilityOf(profile.logOut));
		return profile.logOut.isDisplayed();
	}

	@Step("Verifying user is not signed in")
	public boolean isNotSignedIn() {
		LoginPage login = new LoginPage(driver);
		wait.until(ExpectedConditions.visibilityOf(login.loginButton));
		return login.loginButton.isDisplayed();
	}

	@Step("Verifying user is not signed in")
	public boolean isAtCartPage() {
		CartPage cp = new CartPage(driver);
		wait.until(ExpectedConditions.visibilityOf(cp.deliveryMethodText));
		return cp.deliveryMethodText.isDisplayed();
	}

	// Email generators

	@Step("Generating email adress")
	public String emailAdressGenerator(String type) {
		//String userName = ""+(int)(Math.random()*Integer.MAX_VALUE);
		String userName = "" + date.getTime();
		String emailID ="";
		if (type.equalsIgnoreCase("f")) {
			emailID = userName + "@yopmail.com";
		}
		else if (type.equalsIgnoreCase("p")) {
			emailID = userName + "@mailinator.com";
		}
		else
			System.out.println("Wrong type parameter");

		return emailID;
	}


	public String dateGenerator() {
		String date = dateFormat.format(this.date);
		return date;
	}

	public String timeGenerator() {
		String time = timeFormat.format(this.date);
		return time;
	}



	
	
}
