package com.enetelsolutions.uradisamqa;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.github.cdimascio.dotenv.Dotenv;
import io.qameta.allure.Step;

public class LoginPage extends MainPage{

	Dotenv dotenv = Dotenv.load();
	
	public String BASE_URL = dotenv.get("URL_B2C");
	public String WL_EMAIL = dotenv.get("WL_EMAIL");
	public String TEMP_EMAIL = dotenv.get("TEMP_EMAIL");
	public String VALID_PASS = dotenv.get("PASS");
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}

	String blankEmailMsg = "'Email' adresa je obavezna";
	String blankPassMsg = "'Lozinka' je obavezna";
	String invalidEmailMsg = "'Email' adresa nije validna!";
	String invalidPassMsg = "'Lozinka' mora sadržati minimum 6 karaktera";


	@FindBy(css = ".input-margin-bottom input[name='email']")
	WebElement email;
	
	@FindBy(css = ".input-margin-bottom input[name='password']")
	WebElement password;
	
	@FindBy(css=".login-page [name=Submit]")
	WebElement loginButton;

	@FindBy(css=".login-page #back-button")
	WebElement backButton;


	// Errors

	@FindBy(id = "email-error")
	WebElement emailMessageError;

	@FindBy(id = "password-error")
	WebElement passwordMessageError;


	// Register area

	@FindBy(css="a.switch-register")
	WebElement registerLink;
	

	// INFO page

	@FindBy(className = "info-message-paragraph")
	WebElement infoMessage;

	@FindBy(css="a.link")
	List <WebElement> clickHereLink;



	// PASS RESET page

	@FindBy(css="input[name='customer[password]']")
	WebElement emailField;

	@FindBy(css=".forgot-password-page [name=Submit]")
	WebElement sendBtn;

	@FindBy(css=".forgot-password-page #back-button")
	WebElement backBtn;



	// Methods area

	@Step("Signing in...")
	public void signIn(String mail, String pass) {
		type(mail,email);
			type(pass,password);
			loginButton.click();
	}

	@Step("Go to register form")
	public void goToRegisterForm() {
		registerLink.click();
	}


	@Step("Getting error in email input area")
	public String getEmailError() {
		return emailMessageError.getText();
	}
	
	@Step("Getting error in password input area")
	public String getPassError() {
		return passwordMessageError.getText();
	}

	@Step("Verifying login page is opened")
	public boolean isAtLoginPage() {
		try {
			wait.until(ExpectedConditions.visibilityOf(loginButton));
			return loginButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
		
		
	}

	// INFO PAGE

	@Step("Trying again at wrong login input parameters")
	public void tryAgain() {
		clickHereLink.get(0).click();
	}

	@Step("Getting password reset page")
	public void getpassReset() {
		clickHereLink.get(1).click();
	}

	@Step("Verifying login page is opened")
	public boolean isAtInfoLoginPage() {
		wait.until(ExpectedConditions.visibilityOf(infoMessage));
		return infoMessage.isDisplayed();
	}

	
	// PASS RESET PAGE
	
	@Step("Getting password reseted")
	public void passReset(String email) {
		type(email, emailField);
		sendBtn.click();
	}

	@Step("Signing in Temporary User to clear basket items")
	public void recycleLogin() {
	LoginPage login = new LoginPage(driver);
	profileIcon.click();
	login.signIn(TEMP_EMAIL, VALID_PASS);
	login.signOut();
	}


}
