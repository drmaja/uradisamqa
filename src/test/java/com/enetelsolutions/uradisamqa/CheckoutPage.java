package com.enetelsolutions.uradisamqa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;

public class CheckoutPage extends MainPage {

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }



    @FindBy(css=".switch-container input[value='fizicko']")
    WebElement personalChk;

    @FindBy(css=".switch-container input[value='pravno']")
    WebElement companyChk;

    @FindBy(css = "input#first_name")
    WebElement firstName;

    @FindBy(css = "input#last_name")
    WebElement lastName;

    @FindBy(css = "input#email-main")
    WebElement email;

    @FindBy(css = "input[name='order[mobile]']")
    WebElement phone;

    @FindBy(css = "input[name='order[street]']")
    WebElement street;

    @FindBy(css = "input[name='order[street_number]']")
    WebElement number;

    @FindBy(css = "input#floor")
    WebElement floor;

    @FindBy(css = "input[name='order[flat]']")
    WebElement flat;

    @FindBy(css = "input#city")
    WebElement city;

    @FindBy(id = "first_name-error")
    WebElement firstNameError;

    @FindBy(id = "last_name-error")
    WebElement lastNameError;

    @FindBy(id = "email-main-error")
    WebElement emailError;

    @FindBy(id = "order[mobile]-error")
    WebElement phoneError;

    @FindBy(id = "order[street]-error")
    WebElement streetError;

    @FindBy(id = "order[street_number]-error")
    WebElement numberStreetError;

    @FindBy(id = "city-error")
    WebElement cityError;

    @FindBy(id = "post_code-error")
    WebElement postCodeError;

    String nameErrorMsg = "'Ime' je obavezno";
    String streetErrorMsg = "'Prezime' je obavezno";
    String emailErrorMsgInvalid = "'Email' adresa nije validna!";
    String emailErrorMsgMissing = "'Email' adresa je obavezna";
    String phoneErrorMsgInvalid = "'Telefon' nije validan. (381xxxxxxxxx)";
    String phoneErrorMsgMissing = "'Telefon' je obavezan";
    String streetErrorMsgInvalid = "'Ime ulice' nije validno";
    String streetErrorMsgMissing = "'Ime ulice' je obavezno";
    String numberStreetErrorMsgInvalid = "'Broj ulice' nije validan! (bb, 12a, 34b/lok3, 34/4/25,...)";
    String numberStreetErrorMsgMissing = "'Broj ulice' je obavezan";
    String cityErrorMsgMissing = "'Grad (mesto)' je obavezan'";
    String postalCodeErrorMsgMissing = "'Poštanski broj' je obavezan";





    // @FindBy(css = "div[data-post-code]")
    // List <WebElement> citySelection;


    @FindBy(css = "input#post_code")
    WebElement postalCode;

    // COMPANY DATA
    @FindBy(css = "input#company_name")
    WebElement companyName;

    @FindBy(css = "input#pib")
    WebElement companyTaxNumber;

    @FindBy(css="input#maticni_broj")
    WebElement companyRegisterNumber;

    @FindBy(className = "js-drop-down")
    WebElement paymentSelection;

    @FindBy(css = "input[name='order[accept_terms]']")
    WebElement acceptTerms;

    @FindBy(css = ".form-buttons button[name=Submit]")
    WebElement submit;

    @FindBy(css = "a[onclick='goBack()']")
	WebElement goBackToPreview;






    // Methods

    @Step("user type selection")
    public void selectUserType(int x) {
        if (x == 2) {
            companyChk.click();
        }
        else if (x == 1) {
            personalChk.click();
        }
    }

    @Step("Fill-out form Delivery upon address")
    public void fillFormPersonal() {
        type("Automation", firstName);
        type("Test", lastName);
        String emailGen = emailAdressGenerator("f");
//        String emailGen = "maja.drmoncic@enetelsolutions.com";
        type(emailGen, email);
        type("381648888888", phone);
        type("Goluba Pismonose", street);
        type("99", number);
        type("3", floor);
        type("7", flat);
    }

    @Step("Fill-out form Delivery upon address")
    public void fillFormPersonalCustom(String name, String lastname, String emails, String mobile, String streetname,
                               String snumber,String floornr, String flatnr) {
        type(name, firstName);
        type(lastname, lastName);
        if (emails.equalsIgnoreCase("IGNORE")) {
            String emailGen = emailAdressGenerator("f");
            type(emailGen, email);
        } else {
            type(emails, email);
        }
        type(mobile, phone);
        type(streetname, street);
        type(snumber, number);
        type(floornr, floor);
        type(flatnr, flat);
    }

    @Step("Fill-out form Delivery upon address")
    public void fillFormCompany() {
        type("Company", firstName);
        type("Test", lastName);
        String emailGen = emailAdressGenerator("p");
//        String emailGen = "maja.drmoncic@enetelsolutions.com";
        type(emailGen, email);
        type("381648888888", phone);
        type("Duska Dugouska", street);
        type("99", number);
        type("3", floor);
        type("7", flat);
        type("Kompanija", companyName);
        type("666666666", companyTaxNumber);
        type("99999999", companyRegisterNumber);

    }

    @Step("Fill-out form Pick Up")
    public void fillPickUpFormPersonal() {
        type("Automation", firstName);
        type("Test", lastName);
        String emailGen = emailAdressGenerator("f");
        type(emailGen, email);
        type("381648888888", phone);
    }



    @Step("City selection")
    public void citySelect(String cityenter) {
        type(cityenter , city);
        sleep(1);
        List<WebElement> citySelection = driver.findElements(By.cssSelector("div[data-post-code]"));
        wait.until(ExpectedConditions.visibilityOfAllElements(citySelection));
        for (int i = 0; i < citySelection.size(); i++) {
            String postalCode = citySelection.get(i).getAttribute("data-post-code");
            if (postalCode.equalsIgnoreCase("21235")) {
                try {
                    List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
                    citySelection1.get(i).click();
                } catch (Exception e) {
                    List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
                    citySelection1.get(i).click();
                }

            }
        }
    }

    @Step("Payment selection")
    public void paymentSelection(int option, String type) {
        /*
        0 - Bank Cart -- P, C
        1 - via Account -- P, C - Upon delivery
        2 - Upon delivery (bez rata) -- P
        3 - Upon delivery (na rate) -- P

        */
        if (type.equalsIgnoreCase("f")) {
            paymentSelection.click();
            sleep(1);
            List<WebElement> paymentSelection = driver.findElements(By.cssSelector("ul.customer_payment li"));
            paymentSelection.get(option).click();
        }
        else if(type.equalsIgnoreCase("p")) {
            WebElement paymentSel = driver.findElement(By.className("js-drop-down"));
            paymentSel.click();
            List<WebElement> paymentSelection = driver.findElements(By.cssSelector(".company_payment input[name='order[payment_methodp]']"));
            paymentSelection.get(option).click();

        }

    }

    @Step("Accepting terms")
    public void acceptTerms() {
        sleep(1);
        acceptTerms.click();
    }


    @Step("Submit order")
    public void submitOrder() {
        submit.click();
    }





}