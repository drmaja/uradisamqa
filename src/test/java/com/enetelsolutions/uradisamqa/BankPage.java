package com.enetelsolutions.uradisamqa;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class BankPage extends MainPage {

	public BankPage(WebDriver driver) {
		super(driver);
	}


	// Locators

	// Iznos	RSD	16,00	RSD
	@FindBy(css =".header2TableBar td td span")
	List <WebElement> headerTable;

	@FindBy(id="bname")
	WebElement customerName;

	@FindBy(id="cardnumber")
	WebElement cardNumber;

	@FindBy(id="expmonth")
	WebElement expMonth;

	@FindBy(id="expyear")
	WebElement expYear;

	@FindBy(id="cvm_masked")
	WebElement cvCode;

	@FindBy(id="next")
	WebElement proceed;

	@FindBy(id="cancel")
	WebElement cancel;

	@FindBy(css = "button#proceed-button")
	WebElement proceedButton;

	public String enterBankData(String customer, String cardNum, String code, String process) {
		// process - yes, no
		String amount = getAmount();
		type(customer, customerName);
		type(cardNum, cardNumber);
		monthSelection();
		yearSelection();
		type(code, cvCode);
		if (process.equalsIgnoreCase("yes")) {
			proceed.click();
//			wait.until(ExpectedConditions.visibilityOf(proceedButton));
//			proceedButton.click();
		}
		else {
			cancel.click();
		}
		return amount;
	}


	public String getAmount() {
		String amount = headerTable.get(2).getText();
		return amount;
	}


	public void monthSelection() {
		Select monthSelect = new Select(expMonth);
		monthSelect.selectByValue("0");
	}

	public void yearSelection() {
		Select yearSelect = new Select(expYear);
		yearSelect.selectByValue("1");
	}

	public boolean isAtBankPage() {
		wait.until(ExpectedConditions.visibilityOf(customerName));
		return customerName.isDisplayed();

	}
		
		
}


