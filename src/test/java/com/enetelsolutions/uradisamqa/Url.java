package com.enetelsolutions.uradisamqa;

import org.openqa.selenium.WebDriver;

import io.github.cdimascio.dotenv.Dotenv;

public class Url extends MainPage {

    public Url(WebDriver driver) {
        super(driver);
    }

    String registerURL = "/index.php?mod=customers&op=register";
    String loginURL = "/index.php?mod=customers&op=register";

    String CATEGORY_URL1 = "/mirisi";

    String p1 = "/auto-lak/spray-colors-ral-1023-400ml.html"; // 400895
    String p2 = "/auto-lak/crosol-art-ral-1015-gl-400ml.html"; // 402407
    String p3 = "/klin-za-saenje.html"; // zamenjeno macje oko // 101214
    String p4 = "/plocasti-materijal/blazujka-250x125-cm-18-mm-protivklizna.html"; // pick-up proizvod 102405

    String p5 = "/pribor-za-odrzavanje/brisac-stakla-30cm.html"; // 963765 135
    String p6 = "/mirisi/mirisna-organic-konzerva-michelin-jet-black.html"; // 419308 1080
    String p7 = "/pribor-za-odrzavanje/brisac-stakla-50cm_640156462.html"; // 418935 75
    String p8 = "/pribor-za-odrzavanje/magicna-krpa.html"; // 418508 20
    String p9 = "/pribor-za-odrzavanje/cetka-za-ciscenje-guma-sa-drskom-36cm-michelin.html"; // 420081 44
    String p10 = "/plocasti-materijal/blazujka-250x125-cm-18-mm-protivklizna.html"; // 102405 47
    String p11 = "/cetke-valjci/valjak-akril-kolor-11mm-18cm-rucka.html";  // 415410 23

    String p12 = "/fluo-cevi/led-cev-18w-6500k-1800lm-1-2m.html"; // 710980 pick up proizvod
    String p13 = "/sijalica-za-reflektor-200w-230v-117-6mm-r7s.html"; // 410064 pick up proizvod


    String p14 = "/mirisi/mirisna-bocica-michelin-vanila.html"; // 419305
    String p15 = "/prekidaci/prekidac-og-serijski.html"; // 413242 -- old nr 418508
    String p16 = "/broj-kuni-4-kovani.html"; // 411220 -- old nr 420081
    String p17 = "/zimnica_381/cep-sa-navojem-za-fla-scarone.html"; // 413487






}


