package com.enetelsolutions.uradisamqa;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import io.qameta.allure.Step;

public class CartPage extends MainPage {


	public CartPage(WebDriver driver) {
		super(driver);  
	}


	
	public CartPage (WebDriver driver, int i) {
		super(driver);
		}
		
	

	@FindBy(css = ".opc-product-name a")
	List<WebElement> itemName;

	@FindBy(css = "span.checkout_item_price")
	List<WebElement> itemSinglePrice;

	@FindBy(css = ".opc-product-total-price h4")
	List<WebElement> itemTotalPrice;

	@FindBy(css = ".products_num_wrapper .fnc-cart-quantity")
	List<WebElement> itemQtyInput;

	@FindBy(css = ".products_num_wrapper .fnc-cart-decrement")
	List<WebElement> itemQtyMinus;

	@FindBy(css = ".products_num_wrapper .fnc-cart-increment")
	List<WebElement> itemQtyPlus;

	@FindBy(css = "a.fnc-cart-remove")
	List<WebElement> removeItem;


	@FindBy(css = ".form-buttons .btn-fill")
	List<WebElement> confirmButtons;
	


	// REMOVE FROM CART

	@FindBy(css = ".remodal-wish.remodal-is-opened")
	WebElement modal;

	@FindBy(className = "remodal-cancel")
	WebElement cancel;

	@FindBy(className = "remodal-confirm")
	WebElement confirm;

	@FindBy(css = "button.remodal-close")
	WebElement closeModal;


	// Inserted here to verify preview page

	@FindBy(css = ".suborder-final .suborder-product-total .suborder-price")
	WebElement orderPrice;

	// Inserted here to verify pick-up page
	@FindBy(css = ".selected-city .toggle-icon")
	WebElement cityDropMenu;

	// Inserted here to verify cart page
	@FindBy(css = ".order-method-span-wrapper")
	WebElement deliveryMethodText;


	// Methods

	@Step("Change quantity")
	public void changeQty(int i, String qty) {
		type(qty, itemQtyInput.get(i));
	}


	@Step("Go to cart preview page")
	public void goToCartPreview() {
		confirmButtons.get(0).click();
		wait.until(ExpectedConditions.visibilityOf(orderPrice));
	}

	@Step("Go to cart pick-up page")
	public void goToCartPickUp() {
		confirmButtons.get(1).click();
		wait.until(ExpectedConditions.visibilityOf(cityDropMenu));
	}



	@Step("Searching for product name in cart") // ne koristi se u ovom kodu, prolazi kroz listu imena u korpi i trazi zeljeno ime
	public boolean findProductInCart(String productName) {
		boolean result = false;
		String pName = productName;
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".opc-product-name a")));
		for (int i = 0; i < itemName.size(); i++) {
			String basketNames = itemName.get(i).getText();
			if (pName.equalsIgnoreCase(basketNames)) {
				result = true;
			}
		}
		return result;
	}

	@Step("Change qty for product name in cart")
	public void changeQtyInCart(String productName, String qty) {
		String pName = productName;
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".opc-product-name a")));
		for (int i = 0; i < itemName.size(); i++) {
			List<WebElement> itemNameNew = driver.findElements(By.cssSelector(".opc-product-name a"));
			List<WebElement> itemPriceNew = driver.findElements(By.cssSelector("span.checkout_item_price"));

			String basketNames = itemNameNew.get(i).getText();
			if (pName.equalsIgnoreCase(basketNames)) {
				type(qty, itemQtyInput.get(i));
				itemPriceNew.get(i).click();
				sleep(1.5);
			}
		}
	}


	public void removeFromCart(int index, boolean value) {
		index = index - 1;
		removeItem.get(index).click();
		Actions act = new Actions(driver);
		if (value == true) {
			Action add = act.moveToElement(confirm).build();
			add.perform();
		} else {
			Action add = act.moveToElement(cancel).build();
			add.perform();
		}
		sleep(1);
		Action click = act.click().build();
		click.perform();
		sleep(1);
	}


}
