package com.enetelsolutions.uradisamqa;

import org.testng.annotations.Test;

import java.util.List;

public class ShoppingDeliveryToAddressTest extends InitTest {

    List<CartItem> cartItems;
    List<CartItem> cartPreviewItems;
    Product product1;
    Product product2;


   @Test (description="Normal product in cart - via account")
	public void shopping01RegularProductViaAccount() {
        driver.get(BASE_URL + url.p3);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);
        
        spp.goToCart();
        cartItems = ci.getCartItems();
        //	sa.assertEquals(cartItems.get(index).productName, product1.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.goToCartPreview();
        cpp.getBasketValues();
        sa.assertEquals(cpp.orderProductsTotal, product1.productPrice);

        cpp.goToCheckoutFromPreview();
        checkout.fillFormPersonal();
        checkout.citySelect("Tem");
        checkout.paymentSelection(1, "f");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());

		sa.assertAll();
	}

    @Test (description="Normal and pick up product in cart - via Upon Delivery")
    public void shopping02MixedProductsViaUponDelivery() {
        driver.get(BASE_URL + url.p3);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;

        driver.get(BASE_URL + url.p4);
        int expected2 = spp.cartCountIncrement();
        product2 = spp.addToCartFromSPP();
        int index2 = index + 1;

        sleep(2);
        spp.goToCart();
        cartItems = ci.getCartItems();
        //	sa.assertEquals(cartItems.get(index).productName, product1.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        //	sa.assertEquals(cartItems.get(index2).productName, product2.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index2).productQty, 1);
        sa.assertEquals(cartItems.get(index2).productSinglePrice, product2.productPrice);
        sa.assertEquals(cartItems.get(index2).productTotalPrice, product2.productPrice);

        cart.goToCartPreview();

        cpp.getBasketValues();
        sa.assertEquals(cpp.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cpp.totalOrderVerification());

        cpp.goToCheckoutFromPreview();
        checkout.fillFormPersonal();
        checkout.citySelect("Tem");
        checkout.paymentSelection(2, "f");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());


        sa.assertAll();
    }

    @Test (description="Normal product in cart - via Bank card")
    public void shopping03PaymentByCard() {
        driver.get(BASE_URL + url.p17);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;

        sleep(2);
        spp.goToCart();
        cartItems = ci.getCartItems();
        //	sa.assertEquals(cartItems.get(index).productName, product1.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);


        cart.goToCartPreview();

        cpp.getBasketValues();
        sa.assertEquals(cpp.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cpp.totalOrderVerification());

        cpp.goToCheckoutFromPreview();
        checkout.fillFormPersonalCustom("Maja", "Drmoncic", "tester.enetel@gmail.com",
                "381651234567", "Juzni Bulevar", "10", "3", "11" );
        checkout.citySelect("Tem");
        checkout.paymentSelection(0, "f");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(bank.isAtBankPage());
        bank.enterBankData(BANK_USER, BANK_CARD, BANK_CODE, "yes");
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());
        // validacija da je placanje uspesno, a ne neuspesno
        sa.assertAll();
    }

    @Test (description="Normal product in cart - company")
    public void shopping04Company() {
        driver.get(BASE_URL + url.p3);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);

        spp.goToCart();
        cartItems = ci.getCartItems();
        //	sa.assertEquals(cartItems.get(index).productName, product1.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.goToCartPreview();
        cpp.getBasketValues();
        sa.assertEquals(cpp.orderProductsTotal, product1.productPrice);

        cpp.goToCheckoutFromPreview();

        checkout.selectUserType(2);
        checkout.fillFormCompany();
        checkout.citySelect("Tem");
        checkout.paymentSelection(1, "p");
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderShippingTotal, cpp.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cpp.orderProductsTotal);

        sa.assertEquals(ty.orderTotalAmount, ty.sumSuborderProductTotal());
        sa.assertEquals(ty.orderShippingTotal, ty.sumShippingPrices());
        sa.assertEquals(ty.orderTotalAmount, ty.sumProductsTotal());


        sa.assertAll();
    }

}