package com.enetelsolutions.uradisamqa;

import org.testng.annotations.Test;

import java.util.List;

public class ShoppingPickUpTest extends InitTest {

    List<CartItem> cartItems;
    List<CartItem> cartPickUpItems;
    Product product1;
    Product product2;


    @Test(description = "Normal product in cart - via pick Up")
    public void reservation01RegularProductPickUp() {
        driver.get(BASE_URL + url.p3);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);

        spp.goToCart();
        cartItems = ci.getCartItems();
        //	sa.assertEquals(cartItems.get(index).productName, product1.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.goToCartPickUp();

        sleep(2);
        pickup.selectCity("Novi Sad");
        sleep(2);
        pickup.selectShop("Novi Sad");

        pickup.goToCheckoutFromPickUp();

        checkout.fillPickUpFormPersonal();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        sa.assertAll();

    }

    @Test(description = "Normal and pick up product in cart - via pick Up")
    public void reservation02MixedProductsPickUp() {
        driver.get(BASE_URL + url.p3);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;

        driver.get(BASE_URL + url.p4);
        int expected2 = spp.cartCountIncrement();
        product2 = spp.addToCartFromSPP();
        int index2 = index + 1;

        sleep(2);
        spp.goToCart();
        cartItems = ci.getCartItems();
        //	sa.assertEquals(cartItems.get(index).productName, product1.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        //	sa.assertEquals(cartItems.get(index2).productName, product2.productName); bug sa kapitalizacijom
        sa.assertEquals(cartItems.get(index2).productQty, 1);
        sa.assertEquals(cartItems.get(index2).productSinglePrice, product2.productPrice);
        sa.assertEquals(cartItems.get(index2).productTotalPrice, product2.productPrice);

        cart.goToCartPickUp();

        sleep(2);
        pickup.selectCity("Novi Sad");
        sleep(2);
        pickup.selectShop("Novi Sad");

        pickup.goToCheckoutFromPickUp();

        checkout.fillPickUpFormPersonal();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        sa.assertAll();

    }

}