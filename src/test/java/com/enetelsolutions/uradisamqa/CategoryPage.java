package com.enetelsolutions.uradisamqa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;


public class CategoryPage extends MainPage {

	public CategoryPage(WebDriver driver) {
		super(driver);
	}
	
	
	@FindBy (css = "h1.category-title")
	WebElement categoryTitle;

	
	// Sorting zone
	
	@FindBy (css=".show-sort-box .active_view")
	WebElement sortByPrice;
	
	@FindBy (xpath = "//ul[contains(@class,'show-sort-box')]/*[2]/a")
	WebElement sortByName;

	// Pagination
	
	@FindBy (css=".paginationTG ul li.currentpage")
	WebElement currentPage;
	
	@FindBy (css=".paginationTG ul li a")
	List <WebElement> pagerLinks;
	
	@FindBy (linkText="SLEDEĆA")
	WebElement goToNextPage;

	@FindBy (linkText=">>")
	WebElement goToLastPage;
	


	// Product zone (grid)

	@FindBy(css = "div.product .product-code")
	List <WebElement> productCode; // iscitan kod ima space u sebi, potreban parsing

	@FindBy(css = "div.product .product-image-box a")
	List <WebElement> productImageLink;

	@FindBy(css = "div.product .fnc-compare-checkbox")
	List <WebElement> productCompareBtn;

	@FindBy(css = "div.product h3 a")
	List <WebElement> productName;

	@FindBy(css = "div.product .product-price")
	List <WebElement> productPrice; // npr. 12.999 RSD potreban parsing

	@FindBy(css = "div.product .fnc-cart-btn")
	List <WebElement> productAddToCart;

	@FindBy(css = "div.product .product-wish")
	List <WebElement> saveToWishlist;



	// Methods

	// Product zone methods

	

	@Step("Getting product's code, name and price")
	public Product getProductData (int index) {
		product = new Product();
		product.productCode = productCode.get(index).getText(); // kod proizvoda
		product.productName = productName.get(index).getText(); // ime proizvoda
		product.productPrice = stringToDouble(productPrice.get(index).getText()); // cena proizvoda
		return product;
	}

	@Step("Adding to cart")
	public Product addToCart (int index) {
		index = index - 1;
		product = getProductData(index);
		productAddToCart.get(index).click();
		sleep(2);
		return product;
	}

	// @Step("Getting product code")
	// public String getproductCode(int index) { 
	// 	index = index - 1;
	// 	String[] product = getProductData(index);
	// 	return product[0];
	// }

	// @Step("Getting product name")
	// public String getproductName(int index) { 
	// 	index = index - 1;
	// 	String[] product = getProductData(index);
	// 	return product[1];
	// }

	// @Step("Getting product's code, name and price")
	// public String[] getProductData (int index) {
	// 	String [] product = new String[3];
	// 	product[0] = productCode.get(index).getText(); // kod proizvoda
	// 	product[1] = productName.get(index).getText(); // ime proizvoda
	// 	product[2] = productPrice.get(index).getText(); // cena proizvoda
	// 	return product;
	// }

	// @Step("Adding to cart")
	// public String[] addToCart (int index) {
	// 	index = index - 1;
	// 	String[] product = getProductData(index);
	// 	productAddToCart.get(index).click();
	// 	sleep(2);
	// 	return product;
	// }

	// @Step("Getting product code")
	// public String getproductCode(int index) { 
	// 	index = index - 1;
	// 	String[] product = getProductData(index);
	// 	return product[0];
	// }

	// @Step("Getting product name")
	// public String getproductName(int index) { 
	// 	index = index - 1;
	// 	String[] product = getProductData(index);
	// 	return product[1];
	// }

	// @Step("Getting and parsing product's price")
	// public double getproductPriceParsed(int index) { // metod koji uzima cenu proizvoda i parsira dobijeni string u double
	// 	index = index - 1;
	// 	String[] product = getProductData(index);
	// 	double parsedPrice = 0;
	// 	parsedPrice = Double.parseDouble(product[2]);
	// 	return parsedPrice;
	// }


	/*
	
	
	// FILTERI
	
	
	@FindBy (css=".toggle-box-link h3")
	List <WebElement> filterTitles;
	
	@FindBy (css = "div.filter-box")
	List <WebElement> filterBox;
	
	@FindBy (className="filters_list_box_numbers")
	List <WebElement> filterNumbers;
	
	@FindBy (css="li input[type='checkbox']")
	List <WebElement> filterChkBoxes;
	
	@FindBy (css=".filters .reset-all a")
	WebElement clearFilter; // UKLONI SVE FILTERE
	
	@FindBy(css=".sort-wrap a.fnc-remove-filter")
	List <WebElement> removeFilter; // STICKER
	
////////////////////////////////////////////////////////////////////////////////////////////	
	

	/*
	
	@Step("Sorting")
	public void sortBy(int i) {
		click(sort);
		sleep(1);
		sortList.get(i).click();
	}
	
	@Step("Sort by name")
	public void sortByName() {
		List<String> strList = new ArrayList<String>();
		for (WebElement imenaProizvoda : productName) {
			String ime = imenaProizvoda.getText();
			strList.add(ime);
		}
		Collections.sort(strList);
		for(String str: strList) {
			System.out.println(" "+str);
		}
	}
	
	@Step("Storing names into list and sorting")
	public List<String> getNames() { // uzima nazive proizvoda i sortira
		List<String> strList = new ArrayList<String>();
		for (WebElement imenaProizvoda : productName) {
			String ime = imenaProizvoda.getText().toLowerCase();
			strList.add(ime);
		}
		Collections.sort(strList);
		
//		for(String str: strList) {
//			System.out.println(" "+str);
//		}
		return strList;
	}
	
	@Step("Storing names into list without sorting")
	public List<String> getNamesNoSort() { // uzima nazive proizvoda bez sortiranja
		List<String> strList = new ArrayList<String>();
		for (WebElement imenaProizvoda : productName) {
			String ime = imenaProizvoda.getText();
			strList.add(ime);
		}
		return strList;
	}
	
	
	@Step("Storing prices into list and sorting / no parsing")
	public List<String> getPrices() { // uzima listu cena i sortira bez parsiranja
		List<String> strList = new ArrayList<String>();
		for (WebElement ceneProizvoda : productPrice) {
			String cena = ceneProizvoda.getText();
			strList.add(cena);
		}
		
		Collections.sort(strList);
		
//		for(String str: strList) {
//		System.out.println(" "+str);
//	}
		return strList;
	}
	
	@Step("Storing prices into list with sorting and parsing")
	public List<Double> getPricesParsed() { // uzima listu cena, sortira i parsira
		List<Double> strList = new ArrayList<Double>();
		for (WebElement ceneProizvoda : productPrice) {
			double cena = 0;
			String parseCena = ceneProizvoda.getText().replaceAll("[A-z\\s\\,]*", "");
			cena = Double.parseDouble(parseCena);
			strList.add(cena);
		}
		
		Collections.sort(strList);
		
//		for(String str: strList) {
//		System.out.println(" "+str);
//	}
		return strList;
	}
	
	
	
	
	
	
	
	
	public void goNext() {
		click(sledeca);
	}
	
	@Step("Adding to cart")
	public String addToCart(int i) {
		String productName = "";
		i = i - 1;
		if (i <= productCartBtn.size()) {
			try {
				productCartBtn.get(i).click();
				productName = this.productName.get(i).getText();
			} catch (Exception e) {
				System.out.println("Product doesn't exist");
			}
		}
		else {
			System.out.println("Wrong index number");
		}
		
		return productName;
	}
	
	@Step("Opening Single product page")
	public void openSPP (int i) {
		i = i - 1;
		if (i <= productName.size()) {
			try {
				productName.get(i).click();
			} catch (Exception e) {
				System.out.println("Product doesn't exist");
			}
		}
		else {
			System.out.println("Wrong index number");
		}
	}
	
	@Step("Counting products on current page")
	public int countCurrentPageProducts() { // broji sve
		return productAllGrid.size();
	}
	

	@Step("Go to the next page")
	public void goToNextPage() {
		int pageCounter = 1;
		try {
			while (sledeca.isDisplayed()) {
				goNext();
				pageCounter++;	
			}
			
		} catch (Exception e) {
			System.out.println("Ukupan broj stranica u kategoriji: " + pageCounter);
		}
	}
	
	@Step("Counting products in category")
	public int productsCounter() {
		int counter = productAllGrid.size();
		int pagecounter = 1;
		try {
			while (sledeca.isDisplayed()) {
				goNext();
				pagecounter++;
				counter = counter + productAllGrid.size();
			}
			
		} catch (Exception e) {
		//	System.out.println("Ukupan broj proizvoda: " + counter);
		//	System.out.println("Ukupan broj stranica u kategoriji: " + pagecounter);
		}
		return counter;
	}
	
	@Step("Filter value applying")
	public int filterCheck(int i) {
		i = i - 1;
	//	String filterbroj = filterNumbers.get(i).getText();
		filterChkBoxes.get(i).click();
		int countAfterFilter = productsCounter();
	//	System.out.println("Broj proizvoda za filter vrednost je: " + filterbroj);
	//	System.out.println("Broj proizvoda posle filtriranja je:" + countAfterFilter);
	//	int filterNr = Integer.parseInt(filterbroj);
		return countAfterFilter;
	}
	
	@Step("Getting filter value")
	public int getFilterValueItems(int i) {
		i = i - 1;
		String filterbroj = filterNumbers.get(i).getText();
		int filterNr = Integer.parseInt(filterbroj);
		return filterNr;
	}
	
	@Step("Removing filter value")
	public void removeFilterItem(int i) {
		i = i - 1;
		removeFilter.get(i).click();
		sleep(2);
	}
	
	@Step("Clearing all filters")
	public void clearAllFilters() {
		click(clearFilter);
		sleep(2);
	}
	
	@Step("Verifying presence of category page title")
	public boolean titlePresence() {
		return isDisplayed(pageTitle);
	}
	
	@Step("Verifying presence of sort interface")
	public boolean sortPresence() {
		return isDisplayed(sort);
	}
	
	@Step("Verifying presence of pager")	
	public boolean pagerPresence() {
		return isDisplayed(pager);
	}
	
	@Step("Verifying presence of sort interface")
	public boolean pagerAppear(int x) {
		boolean should = false;
		if (x > 40) {
			should = true;
		}
		else if(x < 0) {
			System.out.println("Empty category");
		}
		return should;
	}
	
	@Step("Verifying presence of filter removal sticker")
	public boolean filterRemovalPresence() {
		return isDisplayed(clearFilter);
	}
	
	@Step("Verifying presence of filters")
	public boolean filterPresence() {
		boolean display = false;
		
		int filters = filterTitles.size();
		if (filters > 1) {
			display = true;
			for (WebElement filteri : filterTitles) {
			//	System.out.println(filteri.getText());
			}
		}
		else {
			display = false;
		}
		return display;
	}
	
	*/
	
}