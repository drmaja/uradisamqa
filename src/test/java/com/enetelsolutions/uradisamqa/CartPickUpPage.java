package com.enetelsolutions.uradisamqa;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class CartPickUpPage extends CartPage {

    public CartPickUpPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".selected-city .toggle-icon")
    WebElement cityDropMenu;

    @FindBy(css = "ul.select-city li")
    List <WebElement> cityList;

    @FindBy(css = "div.pickup-card-wrapper .pickup-card p.pickup-card-name")
    List <WebElement> shopList;


    @FindBy(css = "button.js-submit-pick-up")
    WebElement confirm;

    @FindBy(css = ".form-buttons button[name=Submit]")
    WebElement submit;





    // Methods

    @Step("Selecting city from dropdown")
    public void selectCity(String cityname) {
        cityDropMenu.click();
        wait.until(ExpectedConditions.visibilityOfAllElements(cityList));
        int size = cityList.size();
        for (int i = 0; i < size; i++) {
            String city = cityList.get(i).getText();
            if (city.equalsIgnoreCase(cityname)) {
                cityList.get(i).click();
            }
        }
    }

    @Step("Picking shop")
    public void selectShop(String shopname) {
        int size = shopList.size();
        for (int i = 0; i < size; i++) {
            String shop = shopList.get(i).getText();
            if (shop.equalsIgnoreCase(shopname)) {
                shopList.get(i).click();
            }
        }
    }

    public void goToCheckoutFromPickUp() {
        wait.until(ExpectedConditions.elementToBeClickable(confirm));
        sleep(2);
        js.executeScript("arguments[0].scrollIntoView(true)", confirm);
        confirm.click();
        wait.until(ExpectedConditions.visibilityOf(submit));
    }

}
