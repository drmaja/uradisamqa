package com.enetelsolutions.uradisamqa;

import org.testng.annotations.Test;

import io.qameta.allure.Step;


public class LoginTest extends InitTest {


	
	@Step("Happy path case")
	@Test // HAPPY PATH
		public void us_login01() {
		driver.get( BASE_URL + url.loginURL);
		login.signIn(VALID_EMAIL, VALID_PASS);
		sa.assertTrue(login.isSignedIn());
		login.signOut();
		
		sa.assertAll();
	}

	@Step("Blank email and password")
	@Test(dependsOnMethods = "us_login01") //  BLANK EMAIL AND PASS
	public void us_login02() {
		driver.get( BASE_URL + url.loginURL);
		login.signIn(BLANK, BLANK);
		sa.assertTrue(login.isAtLoginPage());
		sa.assertEquals(login.getEmailError(), login.blankEmailMsg);
		sa.assertEquals(login.getPassError(), login.blankPassMsg);
		
		sa.assertAll();
	}

	@Step("Uncorrect email and valid password")
	@Test(dependsOnMethods = "us_login01") // UNCORRECT_EMAIL, VALID_PASS
	public void us_login03() {
		login.signIn(UNCORRECT_EMAIL, VALID_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Invalid no monkey email and valid password")
	@Test(dependsOnMethods = "us_login01") // INVALID_NO_MONKEY_EMAIL, VALID_PASS
	public void us_login04() {
		login.signIn(INVALID_NO_MONKEY_EMAIL, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}


	@Step("Invalid no dot email and valid password")
	@Test(dependsOnMethods = "us_login01")
	public void us_login05() {

		login.signIn(INVALID_NO_DOT_EMAIL, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Invalid with space email and valid password")
	@Test(dependsOnMethods = "us_login01")
	public void us_login06() {
		login.signIn(INVALID_WITH_SPACE_EMAIL, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());

		sa.assertAll();
	}

	@Step("Blank email and valid password")
	@Test(dependsOnMethods = "us_login01")
	public void us_login07() {
		login.signIn(BLANK, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.blankEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Valid email and wrong password")
	@Test(dependsOnMethods = "us_login01")
	public void us_login08() {
		login.signIn(VALID_EMAIL, UNCORRECT_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		
		sa.assertAll();
	}

	@Step("Valid email and short invalid password")
	@Test(dependsOnMethods = "us_login01")
	public void us_login09() {
		login.signIn(VALID_EMAIL, INVALID_SHORT_PASS);
		sa.assertEquals(login.getPassError(), login.invalidPassMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Valid email and blank password")
	@Test(dependsOnMethods = "us_login01")
	public void us_login10() {	
		login.signIn(VALID_EMAIL, BLANK);
		sa.assertEquals(login.getPassError(), login.blankPassMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Valid email and invalid password with space")
	@Test(dependsOnMethods = "us_login01")
	public void us_login11() {
		login.signIn(VALID_EMAIL, INVALID_WITH_SPACE_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		
		sa.assertAll();
	}

	@Step("Valid email in password input and valid password in email input field")
	@Test(dependsOnMethods = "us_login01")
	public void us_login12() {
		login.signIn(VALID_PASS, VALID_EMAIL);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}


}
