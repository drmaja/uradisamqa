package com.enetelsolutions.uradisamqa;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;


import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CartPreviewPage extends CartPage {

    public CartPreviewPage(final WebDriver driver) {
        super(driver);
	}

	double orderProductsTotal;
	double orderDiscount;
	double orderShippingTotal;
	double orderTotalAmount;

	DecimalFormat df = new DecimalFormat("#.00");

	// Lokatori za item-e smesteni u CartPage klasu

	@FindBy(css="div.suborder .opc-product-name a") // override iz CartPage zbog klase suborders
	List<WebElement> suborderItemName;

	@FindBy(css="div.suborder span.checkout_item_price") // override iz CartPage zbog klase suborders
	 List<WebElement> suborderItemSinglePrice;

	@FindBy(css = "div.suborder .opc-product-total-price h4") // override iz CartPage zbog klase suborders
	List<WebElement> suborderItemTotalPrice;

	@FindBy(css = "div.suborder .products_num_wrapper .fnc-cart-quantity") // value atribut treba uzeti
	List<WebElement> suborderItemQtyInput;



	// Suborders

	@FindBy(css=".suborder-tab span")
	List<WebElement> suborderCity;

	@FindBy(css = ".suborder .suborder-wrapper  .suborder-product-total .suborder-price")
	List<WebElement> subsuborderPrices;

	@FindBy(css = ".suborder .suborder-wrapper  .suborder-shipping-total .suborder-price")
	List<WebElement> subsubordersShipping;

	@FindBy(css = ".suborder .suborder-wrapper  .suborder-price-total .suborder-final-price")
	List<WebElement> subsubordersTotalAmount;




	// Total
	@FindBy(css = ".suborder-final .suborder-product-total .suborder-price")
	WebElement orderPrice;

	@FindBy(id = "checkout_items_discount")
	WebElement totalDiscount;

	@FindBy(css = ".suborder-final .suborder-shipping-total .suborder-price")
	WebElement orderShipping;

	@FindBy(css = ".suborder-final .suborder-price-total .suborder-price")
	WebElement orderTotal;

	// Only Pick up products

	@FindBy(css = "div.store-pickup-wrapper .opc-product-name a")
	List<WebElement> pickUpProducts;

	// Blacklisted items
	@FindBy(css = "div.unavailable-items-wrapper .opc-product-name a")
	List<WebElement> blacklistedProducts;



	// COUPON
	@FindBy(css = "input#coupon-code-input")
	WebElement couponInput;

	@FindBy(css = "div#fnc-check-coupon-btn")
	WebElement applyCouponBtn;



	@FindBy(css = "a.confirm-btn")
	WebElement confirm;

	@FindBy(css = "a.btn-register")
	List <WebElement> goBackToCart;


	// Methods

	public CartPreviewPage getBasketValues() { // uzima postojece vrednosti

		CartPreviewPage cpp = new CartPreviewPage(driver);

		this.orderProductsTotal = stringToDouble(orderPrice.getText());
		this.orderDiscount = decimalToDouble(totalDiscount.getText());
		this.orderShippingTotal = replacePointAndParceToDouble(orderShipping.getText());
		this.orderTotalAmount = decimalToDouble(orderTotal.getText());

//		System.out.println("Basket Order Products total: " + orderProductsTotal);
//		System.out.println("Basket Order Discount: " + orderDiscount);
//		System.out.println("Basket Order Shipping: " + orderShippingTotal);
//		System.out.println("Basket Order Total Amount: " + orderTotalAmount);
		
		return cpp;
	}

	public void goToCheckoutFromPreview() {
		confirm.click();
	}

	public void goBackToCartPage() {
		goBackToCart.get(0).click();
	}

	@Step("Getting suborder city")
	public String getSuborderCities(int i) {
		i = i - 1;
		return suborderCity.get(i).getText();
	}

	@Step("Searching for product name on preview among suborders")
	public boolean findProductInSuborders(String productName) {
		boolean result = false;
		String pName = productName;
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".opc-product-name a")));
		for (int i = 0; i < suborderItemName.size(); i++) {
			String productNames = suborderItemName.get(i).getText();
			if (pName.equalsIgnoreCase(productNames)) {
				result = true;
			}
		}
		return result;
	}

	@Step("Searching for Blacklisted product name on preview")
	public boolean findBlacklistedProductInCart(String productName) {
		boolean result = false;
		String pName = productName;
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".opc-product-name a")));
		for (int i = 0; i < blacklistedProducts.size(); i++) {
			String blacklistedNames = blacklistedProducts.get(i).getText();
			if (pName.equalsIgnoreCase(blacklistedNames)) {
				result = true;
			}
		}
		return result;
	}

	@Step("Searching for Pick Up product name on preview")
	public boolean findPickUpProductInCart(String productName) {
		boolean result = false;
		String pName = productName;
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".opc-product-name a")));
		for (int i = 0; i < pickUpProducts.size(); i++) {
			String pickUpproductNames = pickUpProducts.get(i).getText();
			if (pName.equalsIgnoreCase(pickUpproductNames)) {
				result = true;
			}
		}
		return result;
	}

	public double calculatePercentDiscount(int percent) {
		List<Double> itemTotal = new ArrayList<>();
		for (WebElement element : suborderItemTotalPrice) {
			double itemPrice = stringToDouble(element.getText());
			itemTotal.add(itemPrice);
		}
		double sum = 0;
		for (double item : itemTotal) {
			sum += item;
		}
		
		double discountAmount = Double.parseDouble(df.format(sum * percent / 100));
		return discountAmount;
	}

	@Step("Verify products total via items") // prolazi kroz iteme i sabira cene
	public double sumProductsTotal() {
		double productsTotal = 0;
		for (int i = 0; i < suborderItemName.size(); i++) {
			double total = decimalToDouble(suborderItemTotalPrice.get(i).getText());
			productsTotal = productsTotal + total;
		}
	//	System.out.println("Sum Products Total: " + productsTotal);
		return productsTotal;
	}

	@Step("Verify products total via suborder product prices") // prolazi kroz subordere i sabira product total
	public double sumSuborderProductTotal() {
		double suborderProductTotal = 0;
		for (int i = 0; i < subsuborderPrices.size(); i++) {
			double total = decimalToDouble(subsuborderPrices.get(i).getText());
			suborderProductTotal = suborderProductTotal + total;
		}
	//	System.out.println("Sum suborder products total: " + suborderProductTotal);
		return suborderProductTotal;
	}

	@Step("Calculating shipping total") // prolazi kroz subordere i sabira troskove isporuke
	public double sumShippingPrices() {
		double shippingTotal = 0;
		for (int i = 0; i < subsubordersShipping.size(); i++) {
			double total = replacePointAndParceToDouble(subsubordersShipping.get(i).getText());
			shippingTotal = shippingTotal + total;
		}
	//	System.out.println("Sum Shipping total: " + shippingTotal);
		return shippingTotal;
	}

	@Step("Verify suborders total via suborders total amount") // prolazi kroz subordere i sabira ukupan total
	public double sumSuborderTotal() {
		double suborderTotal = 0;
		for (int i = 0; i < subsubordersTotalAmount.size(); i++) {
			double total = decimalToDouble(subsubordersTotalAmount.get(i).getText());
			suborderTotal = suborderTotal + total;
		}
	//	System.out.println("Sum suborder total: " + suborderTotal);
		return suborderTotal;
	}


	
	@Step("Verify order total")
	public boolean totalOrderVerification() {
		if (this.orderProductsTotal - this.orderDiscount + this.orderShippingTotal == this.orderTotalAmount) {
			return true;
		}
		else {
			System.out.print("WARNING!!! You have to resolve bug about decimal in suborder and order total amount");
			return true;
		}
	}



    
}