package com.enetelsolutions.uradisamqa;

import java.util.concurrent.TimeUnit;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import io.github.cdimascio.dotenv.Dotenv;

public class InitTest {
	static WebDriver driver;

	Dotenv dotenv = Dotenv.load();

	
	public String VALID_EMAIL = dotenv.get("EMAIL");
	public String VALID_PASS = dotenv.get("PASS");
	public String UNCORRECT_EMAIL = dotenv.get("UNCORRECT_EMAIL");
	public String UNCORRECT_PASS = dotenv.get("UNCORRECT_PASS");
	public String INVALID_NO_MONKEY_EMAIL = dotenv.get("INVALID_NO_MONKEY_EMAIL");
	public String INVALID_NO_DOT_EMAIL = dotenv.get("INVALID_NO_DOT_EMAIL");
	public String INVALID_WITH_SPACE_EMAIL = dotenv.get("INVALID_WITH_SPACE_EMAIL");
	public String INVALID_SHORT_PASS = dotenv.get("INVALID_SHORT_PASS");
	public String INVALID_WITH_SPACE_PASS = dotenv.get("INVALID_WITH_SPACE_PASS");
	public String WL_EMAIL = dotenv.get("WL_EMAIL");
	public String TEMP_EMAIL = dotenv.get("TEMP_EMAIL");
	public String VALID_COMPANY_EMAIL = dotenv.get("COMPANY_EMAIL");
	public String BANK_USER = dotenv.get("BANK_USER");
	public String BANK_CARD = dotenv.get("BANK_CARD");
	public String BANK_CODE = dotenv.get("BANK_CODE");
	public static final String BLANK = "";


	private static String testEnvURL = System.getProperty("environment");
	public static String BASE_URL;


	BankPage bank;
	HomePage home;
	LoginPage login;
	CategoryPage category;
	CartItem ci;
	CartPage cart;
	CartPickUpPage pickup;
	CartPreviewPage cpp;
	CheckoutPage checkout;
	ProfilePage profile;
	PromoServicePage pspage;
	Registration register;
	SingleProductPage spp;
	// SuperCategoryPage supercat;
	SoftAssert sa;
	ThankYouPage ty;
	// JavascriptExecutor js;
	Url url;

	@BeforeSuite
	public void setUpSuite() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

		
		
		  // NORMAL CHROME 
//		 driver = new ChromeDriver();

		// HEADLESS MODE 

		// option 1
		 ChromeOptions chromeOptions = new ChromeOptions();
		 chromeOptions.addArguments("--window-size=1920,1080");
		 chromeOptions.addArguments("--start-maximized");
		 chromeOptions.addArguments("--headless");
		 chromeOptions.addArguments("--no-sandbox");
		 chromeOptions.addArguments("--disable-gpu");
		 chromeOptions.addArguments("--allow-insecure-localhost");
		 driver = new ChromeDriver(chromeOptions);
		
		// option 2
		// ChromeOptions chromeOptions = new ChromeOptions();
		// chromeOptions.addArguments("--window-size=1920,1080");
		// chromeOptions.addArguments("--start-maximized");
		// chromeOptions.addArguments("--headless");
		// driver = new ChromeDriver(chromeOptions);

		/*
		 * // FIREFOX *********************
		 * System.setProperty("webdriver.chrome.driver", "/usr/bin/geckodriver"); driver
		 * = new FirefoxDriver();
		 */

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		
	}

	@BeforeSuite
	public String getEnvironment() {

		BASE_URL="";

		if (testEnvURL.equalsIgnoreCase("preprod")) {
			System.out.println("Test scripts are being executed on " + testEnvURL + " environment");
			BASE_URL = "http://uradisam2-preprod.div.int/";

		} else if (testEnvURL.equalsIgnoreCase("release")){
			System.out.println("Test scripts are being executed on " + testEnvURL + " environment");
			BASE_URL = "http://uradisam2-rel.div.int/";

		} else if (testEnvURL.equalsIgnoreCase("test")){
			System.out.println("Test scripts are being executed on " + testEnvURL + " environment");
			BASE_URL = "http://uradisam2-test.div.int/";

		} else {
			System.out.println("Please provide ENVIRONMENT PARAMETER");
		}

		return BASE_URL;
				
	}

	

	@BeforeTest
	public void letMeWaitALittle() {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}

	

	@BeforeTest
	public void instantiateClass() {
	bank = new BankPage(driver);
	home = new HomePage(driver);
	login = new LoginPage(driver);
	category = new CategoryPage(driver);
	cart = new CartPage(driver);
	ci = new CartItem(driver);
	cpp = new CartPreviewPage(driver);
	checkout = new CheckoutPage(driver);
	pickup = new CartPickUpPage(driver);
	profile = new ProfilePage(driver);
	pspage = new PromoServicePage(driver);
	register = new Registration(driver);
	spp = new SingleProductPage(driver);
	// supercat = new SuperCategoryPage(driver);
	sa = new SoftAssert();
	ty = new ThankYouPage(driver);
	// js = (JavascriptExecutor) driver;
	url = new Url(driver);
	}
	
	

	@AfterMethod
	public void afterMethod(ITestResult testResult) {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			System.out.println(testResult.getName() + ": Failed");
		}
		if (testResult.getStatus() == ITestResult.SUCCESS) {
			System.out.println(testResult.getName() + ": Passed");
		}
	}

	/*@AfterSuite
	public void tearDown() {
		driver.quit();
	}*/


	@Step("Sleeping")
	public void sleep(double sec) {
		double seconds = sec * 1000;
		long millis = (long) seconds;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}