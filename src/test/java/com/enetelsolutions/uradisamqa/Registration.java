package com.enetelsolutions.uradisamqa;


import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import io.github.cdimascio.dotenv.Dotenv;
import io.qameta.allure.Step;

public class Registration extends MainPage{

	public Registration(WebDriver driver) {
		super(driver);
	}

	Dotenv dotenv = Dotenv.load();
	public String VALID_PASS = dotenv.get("PASS");


	@FindBy(css = ".input-switch input#fizicko-lice")
	WebElement personalChkBox;

	@FindBy(id = "customer[first_name]")
	WebElement firstname;

	@FindBy(id = "customer[last_name]")
	WebElement lastName;

	@FindBy(id = "customer[email]")
	WebElement email;

	@FindBy(id = "customer[mobile]")
	WebElement phone;

	@FindBy(id = "customer[password]")
	WebElement password;

	@FindBy(id = "customer[confirm_password]")
	WebElement confirmPassword;

	@FindBy(id = "customer[street]")
	WebElement street;

	@FindBy(id="customer[street_number]")
	WebElement streetNumber;

	@FindBy(id="customer[floor]")
	WebElement floorNumber;

	@FindBy(id="customer[flat]")
	WebElement flatNumber;

	@FindBy(id = "city")
	WebElement city;

	@FindBy(className = "search-result")
	List<WebElement> cityList;

	@FindBy(css="input#age")
	WebElement datePicker;

	@FindBy(className = "ui-datepicker-month")
	WebElement monthPicker;

	@FindBy(className = "ui-datepicker-year")
	WebElement yearPicker;

	@FindBy(css="#ui-datepicker-div a.ui-state-default")
	List <WebElement> daysPicker;

	@FindBy(css=".notifications-switch input[name='customer[newsletter]']")
	WebElement newsletterChk;

	@FindBy(css=".notifications-switch input[name='customer[sms_notifications]']")
	WebElement smsChk;

	@FindBy(css=".register-form .login-form-submit-btn button[name='Submit']")
	WebElement registerConfirm;

	@FindBy(css=".register-form a")
	WebElement goBackFromRegister;


	// Pravno lice

	@FindBy(css = ".input-switch input#pravno-lice")
	WebElement companyChkBox;

	@FindBy(css = "input[name='customer[company_name]']")
	WebElement companyName;

	@FindBy(css = "input[name='customer[pib]']")
	WebElement vatNumber;

	@FindBy(css = "input[name='customer[maticni_broj]']")
	WebElement registerNumber;



	// Methods

	@Step("user type selection")
	public void selectUserType(int x) {
		if (x == 2) {
			companyChkBox.click();
		}
		else if (x == 1) {
			personalChkBox.click();
		}
	}


	@Step("Fill-out form Personal")
	public void fillFormPersonal() {
		type("Automation", firstname);
		type("Personal", lastName);
		type(VALID_PASS, password);
		type(VALID_PASS, confirmPassword);
		String emailGen = emailAdressGenerator("f");
//      String emailGen = "maja.drmoncic@enetelsolutions.com";
		type(emailGen, email);
		type("381666666666", phone);
		type("Dositeja Obradovića", street);
		type("9", streetNumber);
		type("3", floorNumber);
		type("7", flatNumber);
	}

	@Step("Fill-out form Personal")
	public void fillFormCompany() {
		type("Automation", firstname);
		type("Personal", lastName);
		type(VALID_PASS, password);
		type(VALID_PASS, confirmPassword);
		String emailGen = emailAdressGenerator("p");
//      String emailGen = "maja.drmoncic@enetelsolutions.com";
		type(emailGen, email);
		type("381647777777", phone);
		type("Njegoseva", street);
		type("11", streetNumber);
		type("3", floorNumber);
		type("7", flatNumber);
		type("ImpoqoSolutions", companyName);
		type("123456789", vatNumber);
		type("98765432", registerNumber);
	}

	@Step("City selection")
	public void citySelect(String cityenter) {
		type(cityenter , city);
		sleep(1);
		List<WebElement> citySelection = driver.findElements(By.cssSelector("div[data-post-code]"));
		wait.until(ExpectedConditions.visibilityOfAllElements(citySelection));
		for (int i = 0; i < citySelection.size(); i++) {
			String postalCode = citySelection.get(i).getAttribute("data-post-code");
			if (postalCode.equalsIgnoreCase("21000")) {
				try {
					List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
					citySelection1.get(i).click();
				} catch (Exception e) {
					List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
					citySelection1.get(i).click();
				}

			}
		}
	}

	@Step("Date of birth enter")
	public void setDateOfBirth() {
		datePicker.click();
		daysPicker.get(6).click();
	}

	@Step("Confirm registration")
	public void confirm() {
		registerConfirm.click();
	}







// Vladina metoda
//	@Step("Izbor grada")
//	public void citySelect() {
//		wait.until(ExpectedConditions.visibilityOf(city));
//		city.click();
//		city.clear();
//		city.sendKeys("Beo");
//		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("search-result")));
//		for (WebElement webElement : cityList) {
//			if (webElement.getText().contains("Beograd")) {
//				webElement.click();
//			}else if (webElement.getText().contains("Beograd Zvezdara")) {
//				webElement.click();
//			}
//		}
//	}



//	@Step("Odabir datuma rodjenja")
//	public void selectDate(){
//		calendar.click();
//		Select select = new Select(year);
//		select.selectByValue("1990");
//		days.get(5).click();
//		kreirajNalog.click();
//	}


//
//	@Step("Checkbox")
//	public void checkBoxClick() {
//		try {
//			action.moveToElement(checkboxPravno).perform();
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		if (!checkboxPravno.isSelected()) {
//			checkboxPravno.click();
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//
//	}
//
//
//

}




